# quodlibet plugins


Plugins for quodlibet music player <https://quodlibet.readthedocs.org/>


## Plugins list


### Audiofeed import unlisten

Replaced by [podcast browser](https://gitlab.com/snouf/quodlibet_podcasts_browser)


### Random if empty

Play a random album with "next" button  if playlist is empty.

Download : <https://gitlab.com/snouf/quodlibet_plugins/raw/master/random_if_empty.py>


## Install a plugin

  1. Copy ``my_plugin.py`` in ``~/.quodlibet/plugins`` (create the
     folder if needed)
  2. Restart QuodLibet
  3. In ``QuodLibet`` open ``Music ⇒ Plugins`` and search the list for
     ``My Plugin``


## Support

gitlab tickets, irc (SnouF @ freenode) or diaspora* (snouf@diasp.eu)
