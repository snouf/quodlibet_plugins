# -*- coding: utf-8 -*-
# Copyright 2016 Jonas Fourquier
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation

import random, threading
from gi.repository import GLib
from quodlibet import app
from quodlibet import qltk
from quodlibet.plugins.events import EventPlugin


class RamdomIfEmpty(EventPlugin):
    PLUGIN_ID = "random_if_empty"
    PLUGIN_NAME = _("Random if empty")
    PLUGIN_DESC = _('Play a random album if playlist is empty.')

    def enabled(self):

        def on_clicked (button) :
            self.random_album()

        playpausebutton = app.window.top_bar.volume.get_parent().get_parent().get_children()[0].get_children()[1]
        self._sig_playpausebutton_toggled = playpausebutton.connect('clicked',on_clicked)

    def disabled(self):
        playpausebutton = app.window.top_bar.volume.get_parent().get_parent().get_children()[0].get_children()[1]
        playpausebutton.disconnect(self._sig_playpausebutton_toggled)

    def random_album (self) :
        if not app.player.song and len(app.window.songlist.get_songs()) == 0 :
            browser = app.window.browser
            keys = browser.list_albums()
            albumlib = app.library.albums
            values = [albumlib[k] for k in keys]
            album = random.choice(values)
            if browser.can_filter_albums():
                browser.filter_albums([album.key])
                GLib.idle_add(self.unpause)
            elif browser.can_filter('albums'):
                browser.filter('album',[album("album")])
                GLib.idle_add(self.unpause)

    def unpause(self):
        try:
            app.player.next()
        except AttributeError:
            app.player.paused = True
